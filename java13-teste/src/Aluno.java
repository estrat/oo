import java.util.ArrayList;
import java.util.List;

public class Aluno implements Comparable<Aluno>{

    private String nome;

    private int idade;

    private boolean devedor;

    private String uf;

    private List<Curso> cursos = new ArrayList<>();


    public Aluno(String nome, int idade, boolean devedor, String uf) {
        this.nome = nome;
        this.idade = idade;
        this.devedor = devedor;
        this.uf = uf;
    }

    public void setCursos(List<Curso> cursos) {
        this.cursos = cursos;
    }

    public List<Curso> getCursos() {
        return cursos;
    }

    public String getNome() {
        return nome;
    }

    public int getIdade() {
        return idade;
    }

    public boolean isDevedor() {
        return devedor;
    }

    public String getUf() {
        return uf;
    }


    @Override
    public String toString() {
        return "Aluno{" +
                "nome='" + nome + '\'' +
                ", idade=" + idade +
                ", devedor=" + devedor +
                ", uf='" + uf + '\'' +
                '}';
    }

    @Override
    public int compareTo(Aluno o) {
        return this.nome.compareTo(o.nome);
    }
}
