import java.util.stream.IntStream;

public class ProcessamentoParalelo {

    public static void main(String[] args) {

//        IntStream.rangeClosed(1, 30).forEach(System.out::println);

        IntStream.rangeClosed(1, 30).parallel().forEach(System.out::println);
    }
}
