import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class TesteJava13 {


    public static void main(String[] args) {

        List<Aluno> alunos = Arrays.asList(
                new Aluno("rafa", 20, false, "DF"),
                new Aluno("matheus", 15, false, "AC"),
                new Aluno("paula", 20, true, "DF"),
                new Aluno("estevao", 10, false, "SP"),
                new Aluno("joao", 30, false, "SP")
        );



        //********************navegar por alunos

//        alunos.forEach(a -> System.out.println(a));

        //********************remover alunos devedores

//        System.out.println(alunos.removeIf(a -> a.isDevedor()));
        //vai dar o unsupported operation
        //alunos = new ArrayList<>(alunos);
//        System.out.println(alunos.removeIf(a -> a.isDevedor()));

        //********************ordenar alunos por nome
//        alunos.sort(Comparator.naturalOrder());
//        alunos.sort(Collections.reverseOrder());
//        alunos.sort(Comparator.comparing(Aluno::getIdade));

        //********************filtrar alunos acima de 18 anos

//        alunos.stream().filter(a -> a.getIdade()>=18);

        //********************achar aluno mais velho

//        System.out.println(alunos.stream().max(Comparator.comparing(Aluno::getIdade)).get());

        //peek

        //perceber que nao vai alterar o alunos original

        //************************guardar alunos em outro lista
//        List<Aluno> alunosMaiores = alunos.stream().filter(a -> a.getIdade() >= 18).collect(Collectors.toList());
//        System.out.println(alunosMaiores);


        //**************************calcular média de idade dos alunos

        //map nao vai funcionar, tem que ser mapToInt

//        System.out.println(alunos.stream().mapToInt(Aluno::getIdade).average().getAsDouble());

        //**************************agrupar os alunos por cidade

//        alunos.stream().collect(Collectors.groupingBy(Aluno::getUf))
//                .entrySet().forEach(e ->{
//            System.out.println("cidade "+ e.getKey());
//
//            System.out.println("alunos"+ e.getValue());
//
//        });
        //


        //********************************* Listar o valor total dos cursos
        List<Curso> cursos = Arrays.asList(
                new Curso("rafa", new BigDecimal("10.54")),
                new Curso("rafa", new BigDecimal("20")),
                new Curso("rafa", new BigDecimal("30.17")),
                new Curso("rafa", new BigDecimal("43.78"))
        );

        BigDecimal total = cursos.stream().map(Curso::getValorTotal).reduce(BigDecimal.ZERO, BigDecimal::add);
        System.out.println(total);


        //********************************** Listar o valor total dos cursos por alunos

        alunos.forEach(a -> {

            a.setCursos(Arrays.asList(new Curso("rafa", new BigDecimal(new Random().nextInt(100))),
                    new Curso("rafa", new BigDecimal("40.32"))));
        });

//        alunos.stream().map(Aluno::getCursos).collect(Collectors.toList());

        total = alunos.stream().map(Aluno::getCursos).flatMap(List::stream).map(Curso::getValorTotal).reduce(BigDecimal.ZERO, BigDecimal::add);
        System.out.println(total);


    }
}
